todo:

not-started:
Allow being able to set the health and model for each NPC invidually
Add Visual Enhacements (Add Filmgrain from ASW, Radial fog and fake god rays (ask Marnamai), CSM from G-String. Look into "Uberlight" and "Volumetrics"?)
Fix "Bad water boarders" bug, fix Viewmodel rotation bug
Add the ability to load custom localization files from within the bsp (would look for the map name, packed into the map like "resource/mapname_english.txt","resource/mapname_russian.txt",ect)
Add the ability to load loose vcd files
Allow use of BINK and AVI videos as textures (like in Portal 2)
Add credits menu on the main menu
Add NPC adjustments for Expert
Run map-specific skillconfig file that pre-adds sk_ to each command, requiring them to be put in like "plr_dmg_357" "40"
Update GameUI to use modified L4D2 res files
Hack in VScript support (VERY MAYBE)
Implement TE120's ToGL renderer and other enhancements/features
Fix barnacle causing crashes with several types of NPCs
Actually implement aim-assist
Implement custom playermodel support
Update gauss gun to use the HL2 jeep gauss effects
Implement prediction for weapons, NPCs and players (VERY, VERY IMPORTANT)

currently-in-progress:
Implement Source Shader Editor
Add Game instructor from ASW
Fix NPCs eyes (lack of blinking+adding unused "diversion" effect)
Add new player/NPC functionality to some weapons (357, harpoon, alyxgun, ect)
Add default working but hidden flags to the FGD (and removing entities and flags that don't work, like weapon_brickbat)
Add ambient_generic fixes
Allow multiple point_cameras in a single map to be active at once
Add Workshop support
Add Discord Rich Presence (scan for: Campaign Name, Map Name, Difficulty and Time Spent)
Fix up and improve map compilers (cubemap fix, static props fix and instance support are main targets)
Hack in Co-Op/Deathmatch
Add linked_portal_door (look at Portal 2)
Fix up and improve Portal features/fix bugs brought in by merging in Portal 1
Fix up and improve HL1 features/fix bugs brought in by merging in Half-Life: Source
Implement ported Opposing Force entities
Add Restored beta NPCs/Weapons
New NPCs and Weapons by the community
Fix up playermodel selector, and modify it to show the models in 3d

done:
Fixed projected textures with up to 9 in a single map
Add a new difficulty (Expert, much harder then hard)
Add Weapons from HL2:DM
Add Working, basic GameUI based main menu 
Have NPCs support more types of weapons (Combine Soldiers being able to use pistols, Police being able to use rifles and shotguns)
Fix Chemical whiteflash bug
Add a map specific viewmodel hand skin override
Add support for CS style ladders
Implement Portal 1 features (portals, npcs, ect)
Add prop_sphere improvements
Fix Vortigaunt AI fucking up and marking all nodes as not vaild to attack from
Add weapons and NPCs from HL:S that don't have HL2 counterparts
Add the logic_difficultytest ent with a single input (Test) that fires a different output depending on the current difficulty (OnEasy, OnNormal, OnHard, OnExpert)
