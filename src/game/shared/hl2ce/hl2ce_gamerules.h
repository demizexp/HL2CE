//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Game rules for Portal.
//
//=============================================================================//

#ifndef hl2ce_GAMERULES_H
#define hl2ce_GAMERULES_H
#ifdef _WIN32
#pragma once
#endif

#include "gamerules.h"
#include "hl2_gamerules.h"
#include "portal_gamerules.h"

#ifdef CLIENT_DLL
	#define CHL2CEGameRules C_HL2CEGameRules
	#define CHL2CEGameRulesProxy C_HL2CEGameRulesProxy
#endif


class CHL2CEGameRulesProxy : public CGameRulesProxy
{
public:
	DECLARE_CLASS( CPortalGameRulesProxy, CGameRulesProxy );
	DECLARE_NETWORKCLASS();
};

class CHL2CEGameRules : public CPortalGameRules
{
public:
	DECLARE_CLASS(CHL2CEGameRules, CPortalGameRules);

	DECLARE_NETWORKCLASS_NOBASE(); // This makes datatables able to access our private vars.
};

#endif
