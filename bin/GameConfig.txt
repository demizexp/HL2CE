"Configs"
{
	"SDKVersion"		"5"
	"Games"
	{
		"HL2CE"
		{
			"GameDir"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\hl2ce"
			"hammer"
			{
				"GameData0"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\sdk_tools\bin\fgd\hl2ce.fgd"
				"TextureFormat"		"5"
				"MapFormat"		"4"
				"DefaultTextureScale"		"0.250000"
				"DefaultLightmapScale"		"16"
				"GameExe"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\hl2.exe"
				"DefaultSolidEntity"		"func_detail"
				"DefaultPointEntity"		"info_player_start"
				"BSP"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\sdk_tools\bin\vbsp.exe"
				"Vis"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\sdk_tools\bin\vvis.exe"
				"Light"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\sdk_tools\bin\vrad.exe"
				"GameExeDir"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition"
				"MapDir"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\hl2ce\mapsrc"
				"BSPDir"		"C:\Program Files (x86)\Steam\steamapps\common\Half-Life 2 Community Edition\hl2ce\maps"
				"CordonTexture"		"BLACK"
				"MaterialExcludeCount"		"0"
			}
		}
	}
}
